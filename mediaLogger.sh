#!/usr/bin/sh

source ~/.config/mediaLogger/config

CURRENT_TIME=$(date -Is)

for FILENAME in "$@"; do
    if [[ $FILENAME == $WATCH_PATH ]]; then
        printf '%s %s\n' "$CURRENT_TIME" "$FILENAME" >> $LOGFILE
    fi
done

nohup `$MEDIA_PLAYER "$@"` > /dev/null 2>&1 &
